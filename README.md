# Ashita-Stuff
![alt text](https://i.imgur.com/umNpFn8.png)

Everything I'm currently using for FFXI emulation and the Ashita client. I do not retain the rights to any of this code. Please use at your own risk.

## Settings
* **Resolution** - My addons are configured to work on a 1440p monitor and will require edits for any other resolutions.

## Issues
* **Support** - Please attempt to make changes on your own before reaching out for assistance with any issues.